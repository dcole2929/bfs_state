(defun farmer-takes-self (state)
	(legal (make-state (opposite (farmer-side state))
						(wolf-side state)
						(goat-side state)
						(cabbage-side state))))

(defun farmer-takes-wolf (state)
	(cond ((equal (farmer-side state) (wolf-side state))
		(legal (make-state (opposite (farmer-side state))
							(opposite (wolf-side state))
							(goat-side state)
							(cabbage-side state))))
	(t nil)))

(defun farmer-takes-goat (state)
	(cond ((equal (farmer-side state) (goat-side state))
		(legal (make-state (opposite (farmer-side state))
							(wolf-side state)
							(opposite (goat-side state))
							(cabbage-side state))))
	(t nil)))

(defun farmer-takes-cabbage (state)
	(cond ((equal (farmer-side state) (goat-side state))
		(legal (make-state (opposite (farmer-side state))
							(wolf-side state)
							(goat-side state)
							(opposite (cabbage-side state)))))
	(t nil)))

(defun make-state (f w g c)
	(list f w g c))

(defun farmer-side (state)
	(nth 0 state))

(defun wolf-side (state)
	(nth 1 state))

(defun goat-side (state)
	(nth 2 state))

(defun cabbage-side (state)
	(nth 3 state))

(defun opposite (state)
	(cond ((equal 'e state) 'w)(t 'e)))

(defun legal (state)
	(cond ((and (not (equal (farmer-side state) (goat-side state))) (or (equal (wolf-side state) (goat-side state)) 
		(equal (goat-side state) (cabbage-side state)))) ())
	(t state)))
